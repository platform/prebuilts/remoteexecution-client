### Remote Execution Client
remoteexecution-client is a tool that can be used to speed up Android Platform builds
when used against a service that implements the [Remote Execution API](https://github.com/bazelbuild/remote-apis).

We plan to open source remoteexecution-client in the future but don't yet have a timeline.
